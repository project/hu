#!/bin/bash

for name in `find -depth -iname "*.po"`; do
  # filename
  echo $name 
  # does not generate messages.mo but prints statistics with a newline
  msgfmt --statistics $name --output-file=/dev/null
done