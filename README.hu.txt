MAGYAR FORDÍTÓI ESZKÖZÖK
===============================================================================

Mivel a magyar fordítói csapat nem dolgozik a fordítás kialakításában,
amíg a Drupal aktuális kiadása fejlesztési fázisban van, ezért itt
csak a fordítók által használt eszközöket lehet megtalálni. Ezek azoknak
lehetnek hasznosak, akik részt szeretnének venni a fordításban.

FORDÍTÁSOK LETÖLTÉSE
===============================================================================

Annak, akit csak a fordítások letöltése érdekel, javasoljuk a http://drupal.hu
címlap felső részét. A fordításról további információ található a
http://drupal.hu/forditasi címen.

AZ ITT TALÁLHATÓ ESZKÖZÖK
===============================================================================

huspell.sh és huspellpo.sh
---------------------------

Shell szkriptek, melyekkel a PO fájlok helyesírás ellenőrzése elvégezhető.
A hunspell nevű programra építenek, ami az ispell helyesírásellenőrző illesztése
a magyar igényekre.

  http://forditas.fsf.hu/huspell-po.html
  http://hunspell.sourceforge.net/
  http://magyarispell.sourceforge.net/

postat.sh
----------

Statisztikai adatokat ad az aktuális könyvtárban és alatta lévő minden PO fájl
fordítási állapotáról (lefordított karaktersorozatok, fuzzy illesztések, nem
lefordított karaktersorozatok).

update.sh és updatever.php
---------------------------

Megadott verziójú Drupal fordítást frissít sablonok alapján. Az update.sh
használja az updatever.php-t arra, hogy a sablonok alapján új adatokat (melyeket
az msgmerge nem emel át) is megkapjuk a fordításban.

Xmodmap
--------

Magyar idézőjeles karakterkiosztás X-et használó rendszerekre (Linux, FreeBSD).
Be kell másolni a felhasználói könyvtárba .Xmodmap néven, és a grafikus felületet
újraindítani, vagy kiadni az alábbi parancsot:

  xmodmap ~/.Xmodmap

xkb-hu
-------

Az xkb-hu az X Window System rendszerekhez készült billentyűzetkiosztás-fájl.
Segítségével X.Org és XFree86 implementációk alatt előcsalható az „ ” (ctrl-e
és a ctrl-r) és a » « (ctrl-E, ctrl-R). Néhány esetben (pl. KDE alatt) a
szohasznalat.txt -ben leírtak nem működnek, mert a környezet felülírja a saját
beállításaival, és később az xmodmap-re nem hallgat. Ekkor jön jól az xkb-hu.

Több módon is fel lehet telepíteni, a legegyszerűbb:

 - Másoljuk be a fájlt az xkb rules könyvtárába „hu” néven
   (felülírva a régit). Például így:

     cp xkb-hu /usr/share/X11/xkb/rules/hu

 - Érvényesítsük a változásokat. Ha az X-et újraindítjuk,
   akkor erre nincs szükség, feltéve, hogy a magyar
   billentyűkiosztás van érvényben. Például így:

     setxkbmap -layout hu -variant nodeadkeys

HASZNOS ESZKÖZÖK MÁSHOL             
===============================================================================

A Drupal alaprendszer magyar csomagjának előállításához a szintén nálunk
kifejlesztett autolocale modult, localized telepítési profilt és a po-packager
szkripteket is használjuk.

  http://drupal.org/project/autolocale
  http://cvs.drupal.org/viewcvs/drupal/contributions/tricks/po-packager/
