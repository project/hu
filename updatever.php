<?php

/**
 * @file Translation header updater (runs on PHP 5)
 *
 * Copies "Generated from file(s)" sections into the translation, from a template
 * and updates Project-Id-Version information in PO header.
 *
 * arguments: "translation file" "template file" "drupal-5.1"
 */
 
$translation = file_get_contents($argv[1]);
$template = file_get_contents($argv[2]);

if ($template && $translation) {
  if (preg_match("!Generated from files?:(.+)msgid \"\"!Us", $template, $found)) {
    $translation = preg_replace(
      "!Generated from files?:(.+)msgid \"\"!Us",
      str_replace("#, fuzzy\n", "", $found[0]),
      $translation
    );
    if (preg_match("!^drupal-(\\d.\\d(.\\d)?)$!", $argv[3], $found)) {
      $translation = preg_replace(
        '!"Project-Id-Version: Drupal ([^\\\\]+)\\\\!',
        '"Project-Id-Version: Drupal ' . $found[1] . '\\',
        $translation
      );
    }
    if ($out = fopen($argv[1], "w")) {
      echo "written\n";
      fwrite($out, $translation);
      fclose($out);
    }
  }
}
