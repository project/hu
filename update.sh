# Ujabb pot fajlok alapjan 'frissiti' a forditast. Parameterkent 
# a drupal verzio konyvtar elotagot varja 'drupal-5.1' formaban

cd $1-hu
for i in *.po; do
  msgmerge -U ${i} ../forrasok/$1-pot/${i}t
  php ../updatever.php ${i} ../forrasok/$1-pot/${i}t $1
done
